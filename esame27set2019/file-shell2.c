#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

/** ATTENZIONE: Svolto durante il tutorato 19-20. Da revisionare **/

#define CMD_BUFFER_SIZE 128
#define MSG_DATA_SIZE 512
#define MAX_FILENAME_SIZE 64
#define MAX_FULL_PATH_SIZE 512
#define TERMINAL_MSG_TYPE 1

typedef struct _message {
    // Message type
    long mtype;

    // Command
    char cmd[CMD_BUFFER_SIZE];

    // Optional fields
    char fileName[MAX_FILENAME_SIZE];
    char c;
} message_t;

int directoryExplorer(uint id, char* path, int msgQueueDescriptor) {
    printf("Sono il figlio #%u, esploro la directory %s\n", id, path);
    message_t msgBuffer;

    long msgType = id + 2;

    DIR* dir = NULL;
    struct dirent* dirPointer = NULL;
    struct stat statBuffer;
    char fullPath[MAX_FULL_PATH_SIZE];

    while(1) {
        // printf("[Figlio #%u] Aspetto comandi...\n", id);

        // Ricezione messaggio
        if(msgrcv(msgQueueDescriptor, &msgBuffer, sizeof(msgBuffer), msgType, 0) == -1) {
            perror("msgrcv");
            exit(-2);
        }

        // printf("[Figlio #%u] Ricevuto messaggio %s %s %c\n", id, msgBuffer.cmd, msgBuffer.fileName, msgBuffer.c);

        // Gestione messaggi
        if(strcmp(msgBuffer.cmd, "quit") == 0) {
            msgBuffer.mtype = TERMINAL_MSG_TYPE;

            if(msgsnd(msgQueueDescriptor, &msgBuffer, sizeof(msgBuffer), 0) < 0) {
                perror("msgsnd");
                return -2;
            }

            break;
        } else if(strcmp(msgBuffer.cmd, "num-files") == 0) {
            unsigned int regularFilesCount = 0;

            // Apertura cartella
            dir = opendir(path);

            if(dir == NULL) {
                fprintf(stderr, "opendir error (%s) : %s\n", path, strerror(errno));
                exit(-4);
            }

            // Conta dei file regolari
            while((dirPointer = readdir(dir)) != NULL) {
                sprintf(fullPath, "%s/%s", path, dirPointer->d_name);

                if(stat(fullPath, &statBuffer)) {
                    perror("stat");
                    exit(-6);
                }

                if(S_ISREG(statBuffer.st_mode)) {
                    ++regularFilesCount;
                }
            }

            if(closedir(dir) != 0) {
                perror("closedir");
                exit(-5);
            }

            sprintf(msgBuffer.cmd, "%u", regularFilesCount);
        } else if(strcmp(msgBuffer.cmd, "total-size") == 0) {
            unsigned int totalSize = 0;

            // Apertura cartella
            dir = opendir(path);
            if(dir == NULL) {
                fprintf(stderr, "opendir error (%s) : %s\n", path, strerror(errno));
                exit(-4);
            }

            // Calcolo somma delle dimensioni
            while((dirPointer = readdir(dir)) != NULL) {
                sprintf(fullPath, "%s/%s", path, dirPointer->d_name);

                if(stat(fullPath, &statBuffer)) {
                    perror("stat");
                    exit(-6);
                }

                if(S_ISREG(statBuffer.st_mode)) {
                    totalSize += statBuffer.st_size;
                }
            }

            if(closedir(dir) != 0) {
                perror("closedir");
                exit(-5);
            }

            sprintf(msgBuffer.cmd, "%u", totalSize);
        } else if(strcmp(msgBuffer.cmd, "search-char") == 0) {
            // TODO: Implementare il comando
            unsigned int occurrences = 0;

            // Apertura cartella
            dir = opendir(path);
            if(dir == NULL) {
                fprintf(stderr, "opendir error (%s) : %s\n", path, strerror(errno));
                exit(-4);
            }

            // Ricerca delle occorrenze
            while((dirPointer = readdir(dir)) != NULL) {
                if(strcmp(dirPointer->d_name, msgBuffer.fileName) != 0) {
                    continue;
                }

                sprintf(fullPath, "%s/%s", path, dirPointer->d_name);

                if(stat(fullPath, &statBuffer)) {
                    perror("stat");
                    exit(-6);
                }

                if(S_ISREG(statBuffer.st_mode)) {
                    int fd = open(fullPath, O_RDONLY);
                    if(fd == -1) {
                        perror("open");
                        exit(-8);
                    }

                    // TODO: Aggiungere controlli sul risultato del mapping
                    char* map = mmap(NULL, statBuffer.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

                    size_t i = 0;
                    while(i < statBuffer.st_size) {
                        if(map[i] == msgBuffer.c) {
                            ++occurrences;
                        }

                        ++i;
                    }

                    munmap(map, statBuffer.st_size);

                    if(close(fd) != 0) {
                        perror("close");
                        exit(-9);
                    }
                }
            }

            if(closedir(dir) != 0) {
                perror("closedir");
                exit(-5);
            }

            sprintf(msgBuffer.cmd, "%u", occurrences);
        } else {
            perror("unknown cmd");
            exit(-3);
        }

        msgBuffer.mtype = TERMINAL_MSG_TYPE;

        if(msgsnd(msgQueueDescriptor, &msgBuffer, sizeof(msgBuffer), 0) < 0) {
            perror("child msgsnd");
            return -2;
        }

        // printf("[Figlio #%u] Risposta inviata: %s %s %c\n", id, msgBuffer.cmd, msgBuffer.fileName, msgBuffer.c);
    }

    return 0;
}

void printCmdFormatError() {
    printf("ERRORE: Formato non valido\n");
}

int terminal(int msgQueueDescriptor, pid_t* children, size_t childrenCount) {
    // printf("[Terminale] Sono il terminale\n");

    char cmdBuffer[CMD_BUFFER_SIZE];

    int id = 0;
    char cmd[CMD_BUFFER_SIZE];
    char fileName[MAX_FILENAME_SIZE];
    char c;

    message_t msgBuffer;
    
    sleep(1);

    while(1) {
        printf("> ");
        fflush(stdout);

        fgets(cmdBuffer, sizeof(char) * CMD_BUFFER_SIZE, stdin);

        if(strlen(cmdBuffer) == 1) {
            continue;
        }

        // TODO: Necessario?
        if(strncmp(cmdBuffer, "quit", 4) == 0) {
            for(int i = 0; i < childrenCount; ++i) {
                msgBuffer.mtype = i + 2;
                strcpy(msgBuffer.cmd, "quit");

                if(msgsnd(msgQueueDescriptor, &msgBuffer, sizeof(msgBuffer), 0) < 0) {
                    perror("msgsnd");
                    return -2;
                }

                // printf("[Terminale] Attendo risposta da parte del figlio...\n");

                if(msgrcv(msgQueueDescriptor, &msgBuffer, sizeof(msgBuffer), TERMINAL_MSG_TYPE, 0) == -1) {
                    perror("msgrcv");
                    exit(-2);
                }
            }

            break;
        }

        if(strncmp(cmdBuffer, "num-files", 9) == 0) {
            // printf("[Terminale] Comando 'num-files'\n");

            int extractedParameters = sscanf(cmdBuffer, "%s %d", cmd, &id);

            if(extractedParameters < 2) {
                printCmdFormatError();
                continue;
            }

            // printf("[Terminale] Parametri: %s %d\n", cmd, id);
        } else if(strncmp(cmdBuffer, "total-size", 10) == 0) {
            // printf("[Terminale] Comando 'total-size'\n");

            int extractedParameters = sscanf(cmdBuffer, "%s %d", cmd, &id);

            if(extractedParameters < 2) {
                printCmdFormatError();
                continue;
            }

            // printf("[Terminale] Parametri: %s %d\n", cmd, id);;
        } else if(strncmp(cmdBuffer, "search-char", 11) == 0) {
            // printf("[Terminale] Comando 'search-char'\n");

            int extractedParameters = sscanf(cmdBuffer, "%s %d %s %c", cmd, &id, fileName, &c);

            if(extractedParameters < 4) {
                printCmdFormatError();
                continue;
            }

            // printf("[Terminale] Parametri: %s %d %s %c\n", cmd, id, fileName, c);

            // Parametri opzionali, utilizzati solo in questa operazione
            strcpy(msgBuffer.fileName, fileName);
            msgBuffer.c = c;
        } else {
            printf("ERRORE: Comando non riconosciuto: %s", cmdBuffer);
            continue;
        }

        if(id >= childrenCount) {
            printf("ERRORE: Impossibile trovare la directory %d\n", id);
            continue;
        }

        // Invio messaggio
        msgBuffer.mtype = (long) (id + 2);
        strcpy(msgBuffer.cmd, cmd);

        // printf("[Terminale] Sto inviando il messaggio %ld %s %s %c\n", msgBuffer.mtype, msgBuffer.cmd, msgBuffer.fileName, msgBuffer.c);

        if(msgsnd(msgQueueDescriptor, &msgBuffer, sizeof(msgBuffer), 0) < 0) {
            perror("msgsnd");
            return -2;
        }

        // printf("[Terminale] Attendo risposta da parte del figlio...\n");

        if(msgrcv(msgQueueDescriptor, &msgBuffer, sizeof(msgBuffer), TERMINAL_MSG_TYPE, 0) == -1) {
            perror("msgrcv");
            exit(-2);
        }

        printf("Risposta: %s\n", msgBuffer.cmd);

        // printf("[Terminale] Ricevuto messaggio %s %s %c\n", msgBuffer.cmd, msgBuffer.fileName, msgBuffer.c);
    }

    /*for(size_t i = 0; i < childrenCount; ++i) {
        waitpid(children[i], NULL, 0);
    }*/

    printf("Uscita...\n");

    return 0;
}

int main(size_t argc, char** argv) {
    if(argc < 2) {
        printf("USO: %s <directory-1> <directory-2> ...\n", argv[0]);
        return -1;
    }

    // Allocazione strutture IPC
    int msgQueueDescriptor = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if(msgQueueDescriptor < 0) {
        perror("msg queue allocation");
        return -1;
    }

    // Creazione processi
    size_t directoriesCount = argc - 1;

    // printf("Creo %lu figli\n", directoriesCount);

    struct stat statBuffer;

    pid_t* children = malloc(sizeof(pid_t) * directoriesCount);

    for(size_t i = 0; i < directoriesCount; ++i) {
        char* directoryPath = argv[i + 1];

        if(stat(directoryPath, &statBuffer) != 0) {
            fprintf(stderr, "directory check error (%s) : %s\n", directoryPath, strerror(errno));
            return -7;
        }

        pid_t pid = fork();

        // Il processo è il figlio e deve esplorare la directory
        if(pid == 0) {
            return directoryExplorer((uint) i, argv[i + 1], msgQueueDescriptor); 
        }

        children[i] = pid;
    }

    // Avvio core del terminale
    int retcode = terminal(msgQueueDescriptor, children, directoriesCount);

    // Deallocazione strutture IPC e chiusura
    if(msgctl(msgQueueDescriptor, IPC_RMID, NULL) != 0) {
        perror("msg queue destruction");
        return -2;
    }

    return retcode;
}