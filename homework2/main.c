/*
    Homework n.2

    Estendere l'esempio 'move.c' visto a lezione per supportare i 2 casi speciali:
    - spostamento cross-filesystem: individuato tale caso, il file deve essere
     spostato utilizzando la strategia "copia & cancella";
    - spostamento di un link simbolico: individuato tale caso, il link simbolico
     deve essere ricreato a destinazione con lo stesso contenuto (ovvero il percorso
     che denota l'oggetto referenziato); notate come tale spostamento potrebbe
     rendere il nuovo link simbolico non effettivamente valido.

    La sintassi da supportare e' la seguente:
     $ homework-2 <pathname sorgente> <pathname destinazione>
*/

/*
    Svolto da Francesco Agati e presentato durante l'incontro di tutorato del 14 Maggio 2020.
    Soluzioni alternative a questi esercizi sono presenti sul sito del Prof. Di Raimondo.
*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libgen.h>
#include <unistd.h>

#define DIM 1024

int main(size_t argc, char* argv[]){

    char  buf[DIM];
    struct stat src, dest;
    char BUF[DIM];
    int fd, fg;
    int size;
    char *path_dest;

    


    if (argc < 3){
        fprintf(stderr,"Inserire argomenti !\n");
        exit(1);
    }

    //lo faccio lstat così se è un link posso vedere a cosa punta
    if(lstat(argv[1], &src)){
        perror(argv[1]);
        exit(1);
    }


    //se è un link faccio mi copio il contenuto del link e lo copio in un link a destinazione, altrimenti vedo se è regolare
    if(S_ISLNK(src.st_mode)){
        printf("Il file è un link, sto procedendo con la copia del link!\n ... \n");

        int size = readlink(argv[1], buf, DIM);
        buf[size]='\0';
        symlink(buf, argv[2]);

    }else if(S_ISREG(src.st_mode)){

            printf("Il file è un file regolare, sto controllando se è sono dello stesso file system\n   ...   \n!");


        //mi vado a vedere a prendere solo la directory del path di destinazione, dato che il file ancora non esiste e darebbe problemi lo ignoro
            char buff2[256];
            strcpy(buff2, argv[2]);
            path_dest=dirname(buff2);

            if(stat(path_dest, &dest) != 0) {
                perror(path_dest);
                exit(1);
            }

            if(dest.st_dev==src.st_dev){

                printf("I due file appartegono allo stesso file system, sto creando un hardlink! ...\n");
                //stesso filesystem, faccio hardlink semplice
                link(argv[1], argv[2]);
            }else
            {
                //faccio la copia classica  con read e write
                printf("I due file appartengono a due file system diversi, sto copiando normalmente!... \n");
                fd = open(argv[1], O_RDONLY);
                fg = open(argv[2], O_CREAT|O_TRUNC|O_WRONLY, 0666);

                do{
                
                size = read(fd, BUF, DIM);
                write(fg, BUF, size);

                }while(size==DIM);


                close(fd);
                close (fg);
            }
    }else{
        //se il file non è un link o non è regolare
        printf("Non è un file regolare!\n");
    }
    
    //facciamo l'unlink del primo file 
    unlink(argv[1]);  

}