#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <time.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

// Svolto durante il tutorato 19-20 di Sistemi Operativi (18/06/2020). Incompleto.
 
#define CHILD_MSG_ID_OFFSET 1
#define MAX_WORD_SIZE 64
#define MAX_LINE_SIZE 2048

typedef struct _request_message {
    long type;
    char word[MAX_WORD_SIZE];
} request_message_t;

typedef struct _result_message {
    long type;
    char word[MAX_WORD_SIZE];
    char line[MAX_LINE_SIZE];
    long line_number;
} result_message_t;

int child(size_t id, char* filePath, int requests_msg_queue_descriptor, int results_msg_queue_descriptor) {
    printf("Sono il figlio %lu, esplorerò il file %s\n", id, filePath);

    request_message_t request_msg;
    result_message_t result_msg;

    // Open file
    int file_descriptor = open(filePath, O_RDONLY);
    if(file_descriptor == -1) {
        perror("child open");

        result_msg.word[0] = '\0';

        if(msgsnd(results_msg_queue_descriptor, &result_msg, sizeof(result_msg) - sizeof(long), 0) != 0) {
            perror("child open error msgsend");
            return -2;
        }

        return -3;
    }

    // Create requests
    const long msg_type = CHILD_MSG_ID_OFFSET + id;
    result_msg.type = msg_type;

    while(1) {
        if(msgrcv(requests_msg_queue_descriptor, &request_msg, sizeof(request_msg) - sizeof(long), msg_type, 0) == -1) {
            perror("child msg rcv");
            return -2;
        }

        if(request_msg.word[0] == '\0') {
            printf("[Child %lu] Non ho più richieste da soddisfare\n", id);

            result_msg.word[0] = '\0';

            if(msgsnd(results_msg_queue_descriptor, &result_msg, sizeof(result_msg) - sizeof(long), 0) != 0) {
                perror("child end msg snd");
                return -2;
            }

            break;
        }

        printf("[Child %lu] Devo cercare la parola %s\n", id, request_msg.word);

        // TODO: Implementare la ricerca delle parole
        // Il modo più comodo per fare la ricerca è sicuramente usare uno stream
        // Ma rispetta il vincolo di leggere il file "solo una volta"?
        // Da chiedere al professore
        strcpy(result_msg.word, request_msg.word);

        if(msgsnd(results_msg_queue_descriptor, &result_msg, sizeof(result_msg) - sizeof(long), 0) != 0) {
            perror("child msg snd");
            return -2;
        }
    }

    return 0;
} 

int parent(char** words, size_t words_count, char** files, size_t files_count, int requests_msg_queue_descriptor, int results_msg_queue_descriptor) {
    printf("[Terminale] Sono il terminale\n");

    request_message_t request_msg;
    result_message_t result_msg;

    // Invio messaggi di richiesta
    for(size_t w = 0; w < words_count; ++w) {
        strcpy(request_msg.word, words[w]);

        for(size_t f = 0; f < files_count; ++f) {
            request_msg.type = CHILD_MSG_ID_OFFSET + f;

            if(msgsnd(requests_msg_queue_descriptor, &request_msg, sizeof(request_msg) - sizeof(long), 0) != 0) {
                perror("[Terminale] request msg send");
                return -3;
            }

            printf("[Terminale] Inviata al figlio con id messaggio %lu la parola %s\n", request_msg.type, request_msg.word);
        }
    }

    // Invio messaggi di fine richieste
    for(size_t f = 0; f < files_count; ++f) {
        request_msg.type = CHILD_MSG_ID_OFFSET + f;
        request_msg.word[0] = '\0';

        if(msgsnd(requests_msg_queue_descriptor, &request_msg, sizeof(request_msg) - sizeof(long), 0) != 0) {
            perror("[Terminale] end request msg send");
            return -3;
        }

        printf("[Terminale] Inviata al figlio con id messaggio %lu il segnale di fine richieste\n", request_msg.type);
    }

    // Ricezione risposte
    size_t active_children_count = files_count;
    while(active_children_count > 0) {
        if(msgrcv(results_msg_queue_descriptor, &result_msg, sizeof(result_msg) - sizeof(long), 0, 0) == -1) {
            perror("parent msg rcv");
            return -2;
        } 

        long child_id = result_msg.type - CHILD_MSG_ID_OFFSET;

        if(result_msg.word[0] == '\0') {
            printf("[Terminale] Il figlio %ld ha terminato il suo lavoro\n", child_id);

            --active_children_count;
            continue;
        }

        printf("[Terminale] Ho ricevuto la risposta dal figlio %ld: parola %s in linea %ld: %s\n",
            child_id, result_msg.word, result_msg.line_number, result_msg.line);

        // TODO: Implementare la raccolta dei risultati
    }

    return 0;
}

int main(size_t argc, char** argv) {
    if(argc < 4) {
        printf("USAGE: %s <word-1> [word-2] [...] @ <file-1> [file-2] [...]\n", argv[0]);
        return 1;
    }

    // Parsing argomenti
    size_t at_index = 2;
    while(at_index < argc) {
        if(argv[at_index][0] == '@' && argv[at_index][1] == '\0') {
            break;
        }

        ++at_index;
    }

    if(at_index == argc) {
        printf("USAGE: %s <word-1> [word-2] [...] @ <file-1> [file-2] [...]\n", argv[0]);
        return 1;
    }

    char** words = argv + 1;
    size_t words_count = at_index - 1;

    char** files = argv + 1 + at_index;
    size_t files_count = argc - at_index - 1;

    printf("Format ok: searching %lu words in %lu files\n", words_count, files_count);

    // Allocazione strutture IPC
    int requests_msg_queue_descriptor = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0660);
    if(requests_msg_queue_descriptor == -1) {
        perror("requests msg queue creation");
        return -2;
    }

    int results_msg_queue_descriptor = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0660);
    if(results_msg_queue_descriptor == -1) {
        perror("results msg queue creation");
        return -2;
    }

    // Creazione processi figli
    pid_t pid;
    for(size_t c = 0; c < files_count; ++c) {
        pid = fork();

        if (pid == 0) { return child(c, files[c], requests_msg_queue_descriptor, results_msg_queue_descriptor); } 
    }

    int retcode = parent(words, words_count, files, files_count, requests_msg_queue_descriptor, results_msg_queue_descriptor);

    // Deallocazione strutture IPC
    // TODO
    if(msgctl(requests_msg_queue_descriptor, IPC_RMID, NULL) != 0) {
        perror("requests msg queue destruction");
        return -2;
    }

    if(msgctl(results_msg_queue_descriptor, IPC_RMID, NULL) != 0) {
        perror("results msg queue destruction");
        return -2;
    }

    return retcode;
}