#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>

/** ATTENZIONE: Svolto durante il tutorato 19-20. Incompleto **/

#define MAX_MSG_DIM 128

#define MSG_MOVE_REQ_BASE_ID 1

#define MSG_MOVE_BASE_ID 3

#define CLOSE_MSG_DATA 0

// #define PLAYER1_WRITE_SEM_ID 0
// #define PLAYER2_WRITE_SEM_ID 0
// #define JUDGE_CHECK_SEM_ID 0

typedef struct {
    long type;
    char text[MAX_MSG_DIM];
} message_t;

// int WAIT(int sem_des, int num_semaforo) {
//     struct sembuf operazioni[1] = {{num_semaforo,-1,0}};
//     return semop(sem_des, operazioni, 1);
// }

// int SIGNAL(int sem_des, int num_semaforo) {
//     struct sembuf operazioni[1] = {{num_semaforo,+1,0}};
//     return semop(sem_des, operazioni, 1);
// }

int judge(pid_t player1_pid, pid_t player2_pid, int msg_queue_id, uint games_count) {
    printf("Sono il giudice, devo arbitrare %u partite\n", games_count);

    uint current_game = 0;

    message_t message;

    while(current_game < games_count) {
        printf("Partita %d / %d\n", current_game + 1, games_count);

        // Invia le richieste di mosse
        printf("Invio richiesta di mossa al player 1...\n");
        message.type = MSG_MOVE_REQ_BASE_ID;
        if(msgsnd(msg_queue_id, &message, sizeof(message) - sizeof(long), 0) == -1) {
            perror("error while sending move request to player1");
            return -1;
        }
        
        printf("Invio richiesta di mossa al player 2...\n");
        message.type = MSG_MOVE_REQ_BASE_ID + 1;
        if(msgsnd(msg_queue_id, &message, sizeof(message) - sizeof(long), 0) == -1) {
            perror("error while sending move request to player2");
            return -1;
        }

        // Attesa risposte
        printf("Attendo le risposte...");

        // Player 1
        int msg_type = MSG_MOVE_BASE_ID;
        if(msgrcv(msg_queue_id, &message, sizeof(message) - sizeof(long), msg_type, 0) == -1) {
            perror("judge error on receive\n");
            return -2;
        }

        // Player 2
        msg_type = MSG_MOVE_BASE_ID + 1;
        if(msgrcv(msg_queue_id, &message, sizeof(message) - sizeof(long), msg_type, 0) == -1) {
            perror("judge error on receive\n");
            return -2;
        }

        ++current_game;
    }

    // Aspetto la terminazione dei processi figli
    waitpid(player1_pid, NULL, 0);
    waitpid(player2_pid, NULL, 0);

    printf("Chiusura giudice...\n");

    return 0;
}

int player(int id, int msg_queue_id) {
    printf("Sono il giocatore %d\n", id);

    message_t message;

    while(1) {
        int msg_type = MSG_MOVE_REQ_BASE_ID + id - 1;

        if(msgrcv(msg_queue_id, &message, sizeof(message) - sizeof(long), msg_type, 0) == -1) {
            fprintf(stderr, "player %d error on receive\n", id);
            return -2;
        }

        printf("Ho ricevuto (%d) una richiesta: %ld\n", id, message.type);
        break;
    }

    printf("Chiusura giocatore %d...\n", id);

    return 0;
}

int scoreboard(pid_t judge_pid) {
    printf("Sono il tabellone\n");

    waitpid(judge_pid, NULL, 0);

    printf("Chiusura tabellone...\n");

    return 0;
}

int main (size_t argc, char** argv) {
    if(argc != 2) {
        printf("USO: %s <numero-partite>\n", argv[0]);
        return -1;
    }

    uint games_count = atoi(argv[1]);

    printf("Numero partite : %d\n", games_count);

    // Strutture IPC

    // Creazione coda di messaggi
    int msg_queue_id = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0660);

    if(msg_queue_id == -1) {
        perror("msg queue creation");
        return -2;
    }

    // Creazione array di semafori
    // int sem_des = semget(IPC_PRIVATE, 4, IPC_CREAT | IPC_EXCL);

    // Creazione processi
    pid_t pid;

    // Il processo originale è il tabellone.
    // Creazione processo player 1
    pid = fork();

    if(pid == -1) { perror("player1 process creation"); return -1; }
    if(pid == 0) { return player(1, msg_queue_id); }
    pid_t player1_pid = pid;

    // Creazione processo player 2
    pid = fork();

    if(pid == -1) { perror("player2 process creation"); return -1; }
    if(pid == 0) { return player(2, msg_queue_id); }
    pid_t player2_pid = pid;

    // Creazione processo giudice
    pid = fork();

    if(pid == -1) { perror("judge process creation"); return -1; }
    if(pid == 0) { return judge(player1_pid, player2_pid, msg_queue_id, games_count); }
    pid_t judge_pid = pid;

    // Avviamo il processo del tabellone
    int retcode = scoreboard(judge_pid);

    // Deallochiamo le risorse di IPC
    if(msgctl(msg_queue_id, IPC_RMID, NULL) == -1) {
        perror("msg queue destruction");
        return -2;
    }

    return retcode;
}