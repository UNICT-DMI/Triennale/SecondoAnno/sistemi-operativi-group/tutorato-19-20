/*
    Homework n.1

    Scrivere un programma in linguaggio C che permetta di copiare un numero
    arbitrario di file regolari su una directory di destinazione preesistente.

    Il programma dovra' accettare una sintassi del tipo:
     $ homework-1 file1.txt path/file2.txt "nome con spazi.pdf" directory-destinazione
*/

/*
    Svolto da Lorenzo Catania durante l'incontro di tutorato del 7 Maggio 2020.
    Il codice va riordinato e rivisto. Soluzioni più 'collaudate' a questi esercizi
    sono presenti sul sito del Prof. Di Raimondo.
*/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>

#define TRANSFER_BUF_SIZE 2048 

int main(size_t argc, char** argv) {
    printf("Num args: %ld\n", argc);

    // Salviamo il puntatore al percorso di destinazione
    char* dest_path = argv[argc-1];

    printf("Dest path: %s\n", dest_path);

    // Creiamo un buffer per le informazioni dei vari file
    struct stat stat_buffer;

    // Controlliamo che la destinazione sia una directory
    stat(dest_path, &stat_buffer);

    if((stat_buffer.st_mode & S_IFDIR) == 0) {
        printf("WARNING: Destination is not a directory, aborting\n");
        return 1;
    }

    // Creiamo un buffer temporaneo per i vari percorsi
    char* file_dest_path = malloc(sizeof(char) * 256);

    // Creiamo un buffer per caricare e spostare i dati dei file
    char* transfer_buffer = malloc(sizeof(char) * TRANSFER_BUF_SIZE);

    for(size_t i = 1; i < argc - 1; ++i) {
        char* file_path = argv[i];

        // Format del percorso di destinazione
        // Possibile ottimizzazione: mantenere il prefisso che rimane sempre uguale (dest_path) e modificare solo il nome del file
        sprintf(file_dest_path, "%s/%s", dest_path, basename(file_path));

        printf("Moving file '%s' to '%s'\n", file_path , file_dest_path);

        // Controlliamo il tipo di file
        stat(file_path, &stat_buffer);

        // printf("Mask: %d\n", stat_buffer.st_mode & S_IFREG);

        if((stat_buffer.st_mode & S_IFREG) == 0) {
            printf("WARNING: Not a regular file, skipping\n");
            continue;
        }

        // Apriamo il file
        int read_fd = open(file_path, O_RDONLY);
        int write_fd = open(file_dest_path, O_CREAT | O_WRONLY, 0700);

        // Spostiamo i dati
        size_t read_bytes = 0;
        size_t written_bytes = 0;

        read_bytes = read(read_fd, transfer_buffer, TRANSFER_BUF_SIZE);

        do {
            printf("Read bytes: %ld\n", read_bytes);

            written_bytes = write(write_fd, transfer_buffer, read_bytes);

            printf("Written bytes: %ld\n", written_bytes);

            if(written_bytes < 0) {
                printf("Error during writing phase");

                return 2;
            }

            read_bytes = read(read_fd, transfer_buffer, TRANSFER_BUF_SIZE);
        } while(read_bytes > 0);

        // Chiudiamo i file descriptor
        close(write_fd);
        close(read_fd);
    }

    free(file_dest_path);
    free(transfer_buffer);
}