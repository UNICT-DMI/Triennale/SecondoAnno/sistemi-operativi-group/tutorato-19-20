#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <time.h>

/** ATTENZIONE: Svolto durante il tutorato 19-20. Da revisionare **/

#define JUDGE_READ_SEM_ID 0
#define PLAYER_1_MOVE_SEM_ID 1
#define PLAYER_2_MOVE_SEM_ID 2
#define SCOREBOARD_READ_SEM_ID 3

/** Helpers semafori **/
int WAIT(int sem_des, int num_semaforo) {
    struct sembuf operazioni[1] = {{num_semaforo,-1,0}};
    return semop(sem_des, operazioni, 1);
}

int SIGNAL(int sem_des, int num_semaforo) {
    struct sembuf operazioni[1] = {{num_semaforo,+1,0}};
    return semop(sem_des, operazioni, 1);
}

/** Struttura dati della memoria condivisa **/
typedef struct _GameStatus {
    char p1Move;
    char p2Move;
    unsigned int winnerId;
    int remainingGames;
} GameStatus_t;

char randomMove() {
    int val = rand() % 3;

    switch(val) {
        case 0: return 'S';
        case 1: return 'C';
        case 2: return 'F';
    }
}

int getWinner(GameStatus_t* gameStatus) {
    if(gameStatus->p1Move == gameStatus->p2Move) {
        return 0;
    }

    switch(gameStatus->p1Move) {
        case 'S':
            switch(gameStatus->p2Move) {
                case 'C': return 2;
                case 'F': return 1;
            }

        case 'C':
            switch(gameStatus->p2Move) {
                case 'S': return 1;
                case 'F': return 2;
            }

        case 'F':
            switch(gameStatus->p2Move) {
                case 'C': return 1;
                case 'S': return 2;
            }
    }
}

/** Funzioni core dei singoli processi **/
int player(unsigned int playerId, GameStatus_t* gameStatus, int semsDescriptor) {
    printf("Sono il giocatore %d\n", playerId);

    // Inizializzazione seme random 
    srand(time(NULL) + playerId);

    //Il ciclo termina quando tutte le partite sono state giocate
    while(gameStatus->remainingGames > 0) {
        // Scrivi la mossa nell'apposito spazio di memoria condivisa
        char move = randomMove();
        printf("[Giocatore %d] La mia prossima mossa è: %c\n", playerId, move);

        switch(playerId) {
            case 1: 
                gameStatus->p1Move = move;
                break;
            case 2: 
                gameStatus->p2Move = move;
                break;
        }

        if(SIGNAL(semsDescriptor, JUDGE_READ_SEM_ID) != 0) {
            perror("player signal to judge");
            exit(-1);
        }

        printf("[Giocatore %d] Ho finito il turno. Attendo istruzioni dal giudice...\n", playerId);

        int semId = playerId == 1 ? PLAYER_1_MOVE_SEM_ID : PLAYER_2_MOVE_SEM_ID;
        if(WAIT(semsDescriptor, semId) != 0) {
            perror("player wait");
            exit(-1);
        }
    }

    printf("Chiusura giocatore %d\n", playerId);
    return 0;
}

int judge(GameStatus_t* gameStatus, int semsDescriptor) {
    printf("Sono il giudice\n");

    // Continua ad arbitrare finchè le partite non finiscono
    while(gameStatus->remainingGames > 0) {
        printf("[Giudice] Attendo le mosse dai giocatori...\n");

        if(WAIT(semsDescriptor, JUDGE_READ_SEM_ID) != 0) {
            perror("first player wait");
            exit(-1);
        }

        if(WAIT(semsDescriptor, JUDGE_READ_SEM_ID) != 0) {
            perror("second player wait");
            exit(-1);
        }

        // Segna la partita giocata
        --(gameStatus->remainingGames);

        // Decreta il vincitore
        int winnerId = getWinner(gameStatus);
        printf("[Giudice] I giocatori hanno finito, ha vinto %d. Lo comunico al tabellone...\n", winnerId);

        gameStatus->winnerId = winnerId;

        // Segnala l'aggiornamento al tabellone ed aspetta la conferma dell'aggiornamento
        if(SIGNAL(semsDescriptor, SCOREBOARD_READ_SEM_ID) != 0) {
            perror("judge signal to scoreboard");
            exit(-1);
        }

        printf("[Giudice] Attendo il segnale dal tabellone...\n");

        if(WAIT(semsDescriptor, JUDGE_READ_SEM_ID) != 0) {
            perror("judge scoreboard update wait");
            exit(-1);
        }

        // Segnala ai giocatori la possibilità di giocare una nuova partita
        if(SIGNAL(semsDescriptor, PLAYER_1_MOVE_SEM_ID) != 0) {
            perror("first judge signal to player");
            exit(-1);
        }

        if(SIGNAL(semsDescriptor, PLAYER_2_MOVE_SEM_ID) != 0) {
            perror("first judge signal to player");
            exit(-1);
        }
    }

    printf("Chiusura giudice\n");

    return 0;
}

int scoreboard(pid_t judgePid, pid_t player1Pid, pid_t player2Pid, GameStatus_t* gameStatus, int semsDescriptor) {
    printf("Sono il tabellone\n");

    unsigned int draws = 0, p1Wins = 0, p2Wins = 0;

    while(gameStatus->remainingGames > 0) {
        printf("[Tabellone] Attendo la segnalazione dei risultati...\n");

        if(WAIT(semsDescriptor, SCOREBOARD_READ_SEM_ID) != 0) {
            perror("scoreboard wait for results");
            exit(-1);
        }

        switch(gameStatus->winnerId) {
            case 0: 
                ++draws;
                break;
            case 1:
                ++p1Wins;
                break;
            case 2: 
                ++p2Wins;
                break;
        }

        printf("[Tabellone] Ho aggiornato il punteggio, segnalo al giudice di continuare...\n");

        if(SIGNAL(semsDescriptor, JUDGE_READ_SEM_ID) != 0) {
            perror("scoreboard signal to judge");
            exit(-1);
        }
    }

    waitpid(judgePid, NULL, 0);
    waitpid(player1Pid, NULL, 0);
    waitpid(player2Pid, NULL, 0);

    printf("[Tabellone] Risultato: P1: %d, P2: %d, Pareggi: %d...\n", p1Wins, p2Wins, draws);

    printf("Chiusura tabellone...\n");

    return 0;
}

int main (size_t argc, char** argv) {
    if(argc != 2) {
        printf("USO: %s <numero-partite>\n", argv[0]);
        return -1;
    }

    uint gamesCount = atoi(argv[1]);

    printf("Numero partite : %d\n", gamesCount);

    // Allocazione strumenti di IPC
    // Memoria condivisa
    int sharedMemDescriptor = shmget(IPC_PRIVATE, sizeof(GameStatus_t), IPC_CREAT | IPC_EXCL | 0600);
    if(sharedMemDescriptor == -1) {
        perror("shared memory allocation");
        return -1;
    }

    // Array di semafori
    int semsDescriptor = semget(IPC_PRIVATE, 4, IPC_CREAT | IPC_EXCL | 0600);
    if(semsDescriptor == -1) {
        perror("semaphores allocation");
        return -1;
    }

    unsigned short initialSemsValues[4] = {0, 0, 0, 0};
    if(semctl(semsDescriptor, 0, SETALL, initialSemsValues) != 0) {
        perror("semaphores initialization");
        return -1;
    }

    GameStatus_t* gameStatus = shmat(sharedMemDescriptor, NULL, 0);
    gameStatus->remainingGames = gamesCount;

    // Il processo originale è il tabellone.

    // Creazione dei processi figli
    pid_t pid;

    // Creazione processo player 1
    pid = fork();

    if(pid == -1) { perror("player1 process creation"); return -1; }
    if(pid == 0) { return player(1, gameStatus, semsDescriptor); }
    pid_t player1Pid = pid;

    // Creazione processo player 2
    pid = fork();

    if(pid == -1) { perror("player2 process creation"); return -1; }
    if(pid == 0) { return player(2, gameStatus, semsDescriptor); }
    pid_t player2Pid = pid;

    // Creazione processo giudice
    pid = fork();

    if(pid == -1) { perror("judge process creation"); return -1; }
    if(pid == 0) { return judge(gameStatus, semsDescriptor); }
    pid_t judgePid = pid;

    // Avviamo il core del tabellone
    int retcode = scoreboard(player1Pid, player2Pid, judgePid, gameStatus, semsDescriptor);

    // Deallocazione degli strumenti di IPC
    printf("Dellocazione strumenti IPC...\n");

    // Memoria condivisa
    if(shmctl(sharedMemDescriptor, IPC_RMID, NULL) != 0) {
        perror("shared memory deallocation");
        return -1;
    }

    // Array di semafori
    if(semctl(semsDescriptor, 0, IPC_RMID, NULL) != 0) {
        perror("semaphores deallocation");
        return -1;
    }

    return retcode;
}