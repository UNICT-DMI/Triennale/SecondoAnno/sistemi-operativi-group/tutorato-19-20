#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <time.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

#define MAX_LEN 1024

// Svolto durante l'incontro di tutorato del 25 Giugno 2020. Da completare e rivedere

/** Helpers semafori **/
int WAIT(int sem_des, int num_semaforo) {
    struct sembuf operazioni[1] = {{num_semaforo,-1,0}};
    return semop(sem_des, operazioni, 1);
}

int SIGNAL(int sem_des, int num_semaforo) {
    struct sembuf operazioni[1] = {{num_semaforo,+1,0}};
    return semop(sem_des, operazioni, 1);
}

int applyFilter(char* str, char* filter) {
    if(filter[0] == '^') {
        char* word = filter + 1;
        size_t wordLength = strlen(word);

        if(wordLength == 0) {
            return -2;
        }

        // printf("Applicazione filtro upper a '%s' su %s\n", word, str);

        char* cursor = str;
        char* occurrence;

        while((occurrence = strstr(cursor, word)) != NULL) {
            for(int i = 0; i < wordLength; ++i) {
                occurrence[i] = toupper(occurrence[i]);
            }

            cursor = occurrence + wordLength;
        }

        return 0;
    }
    
    if(filter[0] == '_') {
        char* word = filter + 1;
        size_t wordLength = strlen(word);

        if(wordLength == 0) {
            return -2;
        }

        // printf("Applicazione filtro lower a '%s' su %s\n", word, str);

        char* cursor = str;
        char* occurrence;

        while((occurrence = strstr(cursor, word)) != NULL) {
            for(int i = 0; i < wordLength; ++i) {
                occurrence[i] = tolower(occurrence[i]);
            }

            cursor = occurrence + wordLength;
        }

        return 0;
    }

    if(filter[0] == '%') {
        return 0;
    }

    return -1;
}

int filter(size_t id, char* filterString, char* sharedMem, int semDesc) {
    // printf("[Filter %lu] Inizializzazione\n", id);

    int process = 1;

    while(process) {
        // printf("[Filter %lu] Attendo permesso di lettura...\n", id);

        WAIT(semDesc, id);
        // printf("[Filter %lu] Leggo la stringa: %s\n", id, sharedMem);

        if(sharedMem[0] == 0 && sharedMem[1] == 'E') {
            // printf("[Filter %lu] Ricevuto segnale di chiusura\n", id);
            process = 0;
        } else {
            int filterResult = applyFilter(sharedMem, filterString);

            if(filterResult != 0) {
                printf("[Filter %lu] WARNING: Errore nell'applicazione del filtro %s (%d)\n", id, filterString, filterResult);
            }
        }

        // printf("[Filter %lu] Do al mio successore il permesso di accedere alla memoria...\n", id);

        SIGNAL(semDesc, id + 1);
    }

    return 0;
}

int terminal(char* filePath, char* sharedMem, size_t filtersCount, int semDesc) {
    // printf("[Terminale] Inizializzazione (%s)\n", filePath);

    // printf("[Terminale] Apertura file...\n");

    FILE* stream = fopen(filePath, "r");
    if(stream == NULL) {
        perror("file stream open");
        return -2;
    }

    while(fgets(sharedMem, sizeof(char) * MAX_LEN, stream) != NULL) {
        // printf("[Terminale] Riga: %s\n", sharedMem);

        SIGNAL(semDesc, 0);

        // printf("Attendo risposta dai filtri...\n");

        WAIT(semDesc, filtersCount);

        // printf("[Terminale] Output: %s\n", sharedMem);
        printf("%s", sharedMem);
    }
    printf("\n");

    // printf("[Terminale] Mando il segnale di chiusura...\n");

    sharedMem[0] = 0;
    sharedMem[1] = 'E';

    SIGNAL(semDesc, 0);

    // printf("Attendo conferma di chiusura dai filtri...\n");

    WAIT(semDesc, filtersCount);

    if(fclose(stream) != 0) {
        perror("file stream close");
        return -2;
    }
}

int main(size_t argc, char** argv) {
    if(argc < 3) {
        printf("USAGE: %s <file1.txt> <filter-1> [filter-2] [...]\n", argv[0]);
        return 1;
    }

    // Allocazione strutture IPC
    int sharedMemDesc = shmget(IPC_PRIVATE, sizeof(char) * MAX_LEN, IPC_CREAT | IPC_EXCL | 0600);
    if(sharedMemDesc == -1) {
        perror("shared memory descriptor creation");
        return -2;
    }

    char* sharedMem = (char*) shmat(sharedMemDesc, NULL, 0);
    if(sharedMem == (void*) -1) {
        perror("shared memory mapping");
        return -2;
    }

    int semDesc = semget(IPC_PRIVATE, argc - 1, IPC_CREAT | IPC_EXCL | 0600);

    // Creazione figli
    pid_t pid;
    for(size_t c = 0; c < argc - 2; ++c) {
        pid = fork();

        if(pid == 0) {
            return filter(c, argv[2 + c], sharedMem, semDesc);
        }
    }

    int retcode = terminal(argv[1], sharedMem, argc - 2, semDesc);

    // Deallocazione strutture IPC
    if(shmctl(sharedMemDesc, IPC_RMID, NULL) != 0) {
        perror("shared memory deallocation");
    }

    if(semctl(semDesc, 0, IPC_RMID, NULL) != 0) {
        perror("semaphores deallocation");
    }

    return retcode;
}